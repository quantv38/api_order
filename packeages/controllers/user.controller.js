"use strict";

const User = require("../models/user.model");
const async = require("async");
const validatior = require("validator");
const utils = require("../services/utils");

exports.list = function (req, res, next) {
  var keyword = req.query.keyword || null;
  var status = req.query.status || null;

  var page_index = req.params.page_index || 1;
  var page_size = req.params.page_size || 10;

  page_index = parseInt(page_index);
  page_size = parseInt(page_size);
  var offset = page_size * (page_index - 1);

  var query = {};
  var status_deleted = [2];

  if (status != null) {
    query.status = status;
  }

  query.status = { $nin: status_deleted };

  //check theo tên khách mời
  if (keyword != null) {
    keyword = utils.escapeRegExp(keyword);
    query["$and"] = [
      {
        $or: [
          { username: new RegExp(keyword, "i") },
          { fullname: new RegExp(keyword, "i") },
          { mobile: new RegExp(keyword, "i") },
        ],
      },
    ];
  }

  console.log(query);
  async.series(
    [
      function (cb) {
        User.find()
          .where(query)
          .sort({ created_at: -1 })
          .limit(page_size)
          .skip(offset)
          .lean()
          .exec(function (err, users) {
            if (err) {
              return res.json({
                code: 400,
                message: err.message,
                data: null,
              });
            } else {
              cb(null, {
                code: 200,
                message: "Danh sach user",
                data: users,
              });
            }
          });
      },
      function (cb) {
        User.where(query).count(function (err, count) {
          if (err) {
            cb(null, 0);
          } else {
            cb(null, count);
          }
        });
      },
    ],

    function (err, results) {
      results[0].meta = {
        page_index: page_index,
        page_size: page_size,
        total_record: results[1],
        total_page: Math.ceil(results[1] / page_size),
      };
      res.json(results[0]);
    }
  );
};

exports.store = function (req, res, next) {
  var username = req.body.username;
  var password = req.body.password;
  var fullname = req.body.fullname || null;
  var mobile = req.body.mobile || null;
  var status = req.body.status || 0;

  var user = new User();
  user.username = username;
  user.password = password;
  user.fullname = fullname;
  user.mobile = mobile;
  user.status = status;

  user.save(function (err, result) {
    console.log(result);
    if (err) {
      return res.json({
        code: 500,
        message: err.message,
        data: null,
      });
    } else {
      return res.json({
        code: 200,
        message: "Them moi user thanh cong",
        data: result,
      });
    }
  });
};

exports.update = function (req, res, next) {
  var params = {
    _id: req.body._id,
    username: req.body.username,
    fullname: req.body.fullname || null,
    mobile: req.body.mobile || null,
    status: req.body.status || 0,
  };

  async.waterfall(
    [
      function (cb) {
        console.log(params._id);
        if (validatior.isEmpty(params._id)) {
          cb({
            code: 400,
            message: "Truyen thieu tham so _id",
            data: null,
          });
        } else {
          cb();
        }
      },

      function (cb) {
        if (!validatior.isMongoId(params._id)) {
          cb({
            code: 400,
            message: "_id khong hop le",
            data: null,
          });
        } else {
          cb();
        }
      },

      function (cb) {
        if (validatior.isEmpty(params.username)) {
          cb({
            code: 400,
            message: "Thiếu tham số username",
            data: null,
          });
        } else {
          cb();
        }
      },
      function (cb) {
        cb();
      },
    ],
    function (err, results) {
      if (err) {
        return res.json({
          code: 400,
          message: err.message,
          data: null,
        });
      } else {
        User.findOne({ _id: params._id }).exec(function (err, user) {
          if (user == null) {
            return res.json({
              code: 400,
              message: err.message,
              data: null,
            });
          } else {
            user.username = params.username;
            user.fullname = params.fullname;
            user.mobile = params.mobile;
            user.status = params.status;

            user.save((err, result) => {
              if (err) {
                return res.json({
                  code: 400,
                  message: err.message,
                  data: null,
                });
              } else {
                return res.json({
                  code: 200,
                  message: "update thành công",
                  data: result,
                });
              }
            });
          }
        });
      }
    }
  );
};

exports.delete = function (req, res, next) {
  var _id = req.body._id;

  if (validatior.isEmpty(_id)) {
    return res.json({
      code: 400,
      message: "Truyền thiếu tham số _id",
      data: null,
    });
  }

  if (!validatior.isMongoId(_id)) {
    return res.json({
      code: 400,
      message: "_id không hợp lệ",
      data: null,
    });
  }

  User.findOne({ _id: _id }).exec(function (err, user) {
    if (err) {
      return res.json({
        code: 400,
        message: err.message,
        data: null,
      });
    } else {
      if (!user) {
        return res.json({
          code: 400,
          message: "user không tồn tại.",
          data: null,
        });
      } else {
        user.status = 2;
        user.save(function (err, result) {
          if (err) {
            return res.json({
              code: 400,
              message: err.message,
              data: null,
            });
          } else {
            return res.json({
              code: 200,
              message: "Xóa thành công",
              data: null,
            });
          }
        });
      }
    }
  });
};
