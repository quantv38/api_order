const mongoose = require("mongoose");

const { Schema } = mongoose;
const ObjectId = Schema.ObjectId;

const FoodSchema = new Schema({
  name: { type: String, trim: true, required: "ERROR_NAME_MISSING" },
  code: { type: String, trim: true, required: true },
  category: {
    type: ObjectId,
    ref: "Category",
    index: true,
    trim: true,
    default: null,
  },
  price: { type: Number, require: "ERROR_PRICE_MISSING" },
  status: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  update_at: { type: Date, default: Date.now },
});

/**
 * pre-save hook
 */

FoodSchema.pre("save", function (next) {
  this.update_at = Date.now();
  next();
});

module.exports = mongoose.model("Food", FoodSchema);
