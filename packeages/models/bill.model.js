const mongoose = require("mongoose");

const { Schema } = mongoose;
const ObjectId = Schema.ObjectId;

const BillSchema = new Schema({
  table: {
    type: ObjectId,
    ref: "Table",
    index: true,
    trim: true,
    required: "ERROR_TABLE_MISSING",
  },
  customer_name: { type: String, require: "ERROR_CUSTOMER_NAME_MISSING" },
  customer_mobile: { type: String, default: null },
  food: [
    {
      food_detail: {
        type: ObjectId,
        ref: "Food",
        index: true,
        trim: true,
        default: null,
      },
      quantity: { type: Number, default: 1 },
    },
  ],
  status: { type: Number, default: 0 },
  created_by: {
    type: ObjectId,
    ref: "User",
    index: true,
    trim: true,
    default: null,
  },
  created_at: { type: Date, default: Date.now },
  update_at: { type: Date, default: Date.now },
});

/**
 * pre-save hook
 */

BillSchema.pre("save", function (next) {
  this.update_at = Date.now();
  next();
});

module.exports = mongoose.model("Bill", BillSchema);
