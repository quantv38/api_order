"use strict";

const express = require("express");
const router = express.Router();
const foodController = require("../controllers/food.controller");

router.get("/list", foodController.list);
router.post("/store", foodController.store);
router.post("/update", foodController.update);
router.post("/delete", foodController.delete);

module.exports = router;
