"use strict";

const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.controller");

router.get("/list", userController.list);
router.post("/store", userController.store);
router.post("/update", userController.update);
router.post("/delete", userController.delete);

module.exports = router;
