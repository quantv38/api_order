"use strict";

const express = require("express");
const router = express.Router();
const billController = require("../controllers/bill.controller");

router.get("/list", billController.list);
router.post("/store", billController.store);
router.post("/update", billController.update);
router.post("/update_food", billController.update_food);
router.post("/update_status", billController.update_status);
router.post("/delete", billController.delete);

module.exports = router;
