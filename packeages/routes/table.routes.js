"use strict";

const express = require("express");
const router = express.Router();
const tableController = require("../controllers/table.controller");

router.get("/list", tableController.list);
router.post("/store", tableController.store);
router.post("/update", tableController.update);
router.post("/delete", tableController.delete);

module.exports = router;
